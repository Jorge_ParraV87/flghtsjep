﻿using FlySearch.Core.DTOs;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace FlySearch.Web.Controllers
{
    public class VFlightTransportController : Controller
    {
        private readonly IHttpClientFactory _ClientFactory;

        public VFlightTransportController(IHttpClientFactory clientFactory)
        {
            _ClientFactory = clientFactory;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Details()
        {

            List<VFlightTransportDTO> flightList = new List<VFlightTransportDTO>();
            using (var httpClient = new HttpClient())
            {
                using (var apiPetition = await httpClient.GetAsync("https://localhost:44305/api/VFlightTransport"))
                {
                    string apiResponse = await apiPetition.Content.ReadAsStringAsync();
                    flightList = JsonConvert.DeserializeObject<List<VFlightTransportDTO>>(apiResponse);
                }
            }
            return Json(flightList);

        }


    }
}
