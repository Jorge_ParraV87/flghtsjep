﻿var rut = rut ||
    new function () {

        this.urlFlightTransport = "";
    };


$(document).ready(function () {
    ObtainFlies();
});

function ObtainFlies()
{
    $.post(rut.urlFlightTransport,

        function (data)
        {
            debugger;

            var table = new Tabulator("#TabulatorFligth", {
                height: "auto",
                layout: "fitColumns",
                data: data,
                pagination: "local",
                paginationSize: 10,
                placeholder: "No Data Available",
                columns: [
                    { title: "Departure station", field: "departureStation", sorter:"string", headerFilter: true},
                    { title: "Arrival station", field: "arrivalStation", sorter: "string", headerFilter: true },
                    { title: "Departura date", field: "depDate", sorter: "string", headerFilter: true },
                    { title: "Departura hour", field: "depHour", sorter: "string", headerFilter: true },
                    { title: "Name transport", field: "nameTransport", sorter: "string", headerFilter: true},
                    { title: "Flight number", field: "flightNumber", sorter: "string", headerFilter: true },
                    { title: "Price", field: "price", sorter: "string", headerFilter: true },
                    { title: "Currency", field: "currency", sorter: "string", headerFilter: true },
                ],
            });

        }
    )
}