using FlySearch.Core.Interfaces;
using FlySearch.Infrastructure.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace FlySearch.test
{
    [TestClass]
    public class VflightInterfaceTest
    {
        private static readonly IServiceCollection _serviceCollection = new ServiceCollection();

        private static IVflightTransportInterface _sut;
        //private readonly Mock<IVflightTransportInterface> mockVFlighTransport = new Mock<IVflightTransportInterface>();
        [TestInitialize]
        public void TestInitialize()
        {
           // IConfiguration configuration = new ConfigurationBuilder();
            _serviceCollection.AddTransient<IVflightTransportInterface, VFlightTransportRepositories>();

            var serviceScope = _serviceCollection.BuildServiceProvider().CreateScope();
            _sut = serviceScope.ServiceProvider.GetService<IVflightTransportInterface>();
        }


        [TestMethod]
        public async Task ReturnDataVflightTransport()
        { 
            //Act
            var result = await _sut.GetVFlights();
            Assert.IsNull(result);
            
        }
    }
}
