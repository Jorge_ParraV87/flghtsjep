﻿using FlySearch.Core.Interfaces;
using FlySearch.Infrastructure.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace FlySearch.test
{
    [TestClass]
    public class FlightTest
    {
        private static readonly IServiceCollection _serviceCollection = new ServiceCollection();

        private static IFlightInterface _sut;
        //private readonly Mock<IVflightTransportInterface> mockVFlighTransport = new Mock<IVflightTransportInterface>();
        [TestInitialize]
        public void TestInitialize()
        {
            // IConfiguration configuration = new ConfigurationBuilder();
            _serviceCollection.AddTransient<IFlightInterface, FlightRepositories>();

            var serviceScope = _serviceCollection.BuildServiceProvider().CreateScope();
            _sut = serviceScope.ServiceProvider.GetService<IFlightInterface>();
        }


        [TestMethod]
        public async Task ReturnDataFlight()
        {
            //Act
            var result = await _sut.GetFlights();
            Assert.IsNull(result);

        }
    }
}
