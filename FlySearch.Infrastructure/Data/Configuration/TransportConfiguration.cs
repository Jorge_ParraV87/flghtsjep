﻿using FlySearch.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FlySearch.Infrastructure.Data.Configuration
{
    class TransportConfiguration : IEntityTypeConfiguration<Transport>
    {
        public void Configure(EntityTypeBuilder<Transport> builder)
        {
            builder.HasKey(e => e.IdTransport);

            builder.Property(e => e.IdTransport).HasColumnName("Id_transport");

            builder.Property(e => e.FlightNumber)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false);

            builder.Property(e => e.NameTransport)
                .IsRequired()
                .HasMaxLength(150)
                .IsUnicode(false);

        }
    }
}
