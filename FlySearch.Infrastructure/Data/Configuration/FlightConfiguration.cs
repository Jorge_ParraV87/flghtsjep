﻿using FlySearch.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FlySearch.Infrastructure.Data.Configuration
{
    class FlightConfiguration : IEntityTypeConfiguration<Flight>
    {
        public void Configure(EntityTypeBuilder<Flight> builder)
        {
            builder.HasKey(e => e.IdFlight);

            builder.Property(e => e.IdFlight).HasColumnName("Id_flight");

            builder.Property(e => e.ArrivalStation)
                .HasMaxLength(150)
                .IsUnicode(false);

            builder.Property(e => e.Currency)
                .HasMaxLength(150)
                .IsUnicode(false);

            builder.Property(e => e.DepartureDate).HasColumnType("datetime");

            builder.Property(e => e.DepartureStation)
                .IsRequired()
                .HasMaxLength(150)
                .IsUnicode(false);

            builder.Property(e => e.IdTransport).HasColumnName("Id_transport");

            builder.Property(e => e.Price).HasColumnType("decimal(18, 2)");

            builder.HasOne(d => d.IdTransportNavigation)
                .WithMany(p => p.Flight)
                .HasForeignKey(d => d.IdTransport)
                .HasConstraintName("FK_Flight_Transport");
        }
    }
}
