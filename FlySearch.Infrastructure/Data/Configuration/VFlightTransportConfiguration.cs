﻿using FlySearch.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FlySearch.Infrastructure.Data.Configuration
{
    class VFlightTransportConfiguration : IEntityTypeConfiguration<VFlightTransport>
    {
        public void Configure(EntityTypeBuilder<VFlightTransport> builder)
        {
            builder.HasNoKey();

            builder.ToView("V_Flight_Transport");

            builder.Property(e => e.ArrivalStation)
                .HasMaxLength(150)
                .IsUnicode(false);

            builder.Property(e => e.Currency)
                .HasMaxLength(150)
                .IsUnicode(false);

            builder.Property(e => e.DepDate)
                .HasMaxLength(10)
                .IsUnicode(false);

            builder.Property(e => e.DepHour)
                .HasMaxLength(7)
                .IsUnicode(false);

            builder.Property(e => e.DepartureDate).HasColumnType("datetime");

            builder.Property(e => e.DepartureStation)
                .IsRequired()
                .HasMaxLength(150)
                .IsUnicode(false);

            builder.Property(e => e.FlightNumber)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false);

            builder.Property(e => e.IdFlight).HasColumnName("Id_flight");

            builder.Property(e => e.IdTransport).HasColumnName("Id_transport");

            builder.Property(e => e.NameTransport)
                .IsRequired()
                .HasMaxLength(150)
                .IsUnicode(false);

            builder.Property(e => e.Price).HasColumnType("decimal(18, 2)");
        }
    }
}
