﻿using System;
using FlySearch.Core.Entities;
using FlySearch.Infrastructure.Data.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FlySearch.Infrastructure.Data
{
    public partial class FlySearchDBContext : DbContext
    {
        public FlySearchDBContext()
        {
        }

        public FlySearchDBContext(DbContextOptions<FlySearchDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Flight> Flight { get; set; }
        public virtual DbSet<Transport> Transport { get; set; }
        public virtual DbSet<VFlightTransport> VFlightTransport { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=LAPTOP-PMK3LVT0;Database=FlySearchDB;user id=sa;password=1987");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new FlightConfiguration());
            modelBuilder.ApplyConfiguration(new TransportConfiguration());
            modelBuilder.ApplyConfiguration(new VFlightTransportConfiguration());
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
