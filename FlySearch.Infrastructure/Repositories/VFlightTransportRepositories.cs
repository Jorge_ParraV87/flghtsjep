﻿using AutoMapper;
using FlySearch.Core.DTOs;
using FlySearch.Core.Interfaces;
using FlySearch.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlySearch.Infrastructure.Repositories
{
    public class VFlightTransportRepositories : IVflightTransportInterface
    {
        private readonly FlySearchDBContext _FlyDbContext;
        private readonly IMapper _mapper;

        public VFlightTransportRepositories(FlySearchDBContext _FlyDb, IMapper mapper)
        {
            _FlyDbContext = _FlyDb;
            _mapper = mapper;
        }

        async Task<List<VFlightTransportDTO>> IVflightTransportInterface.GetVFlights()
        {
            var listVFlight = await _FlyDbContext.VFlightTransport.ToListAsync();

            var listVFlightDTO = _mapper.Map<List<VFlightTransportDTO>>(listVFlight);

            return listVFlightDTO;
        }
    }
}
