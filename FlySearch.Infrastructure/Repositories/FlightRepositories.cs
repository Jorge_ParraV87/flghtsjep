﻿using AutoMapper;
using FlySearch.Core.DTOs;
using FlySearch.Core.Entities;
using FlySearch.Core.Interfaces;
using FlySearch.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FlySearch.Infrastructure.Repositories
{
    public class FlightRepositories: IFlightInterface
    {
        private readonly FlySearchDBContext _FlyDbContext;
        private readonly IMapper _mapper;

        public FlightRepositories(FlySearchDBContext _FlyDb, IMapper mapper)
        {
            _FlyDbContext = _FlyDb;
            _mapper = mapper;
        }

        async Task<List<FlightDTO>> IFlightInterface.GetFlights()
        {
            var listFlight = await _FlyDbContext.Flight.ToListAsync();

            var listFlightDTO = _mapper.Map<List<FlightDTO>>(listFlight);


            return listFlightDTO;
        }


        async Task<FlightDTO> IFlightInterface.GetFlightsId(int id)
        {
            var FirstFlight = await _FlyDbContext.Flight.FirstOrDefaultAsync(x => x.IdFlight == id);

            var FirstFlightDTO = _mapper.Map<FlightDTO>(FirstFlight);

            return FirstFlightDTO;
        }
    }
}
