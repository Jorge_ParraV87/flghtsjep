﻿using AutoMapper;
using FlySearch.Core.DTOs;
using FlySearch.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlySearch.Infrastructure.Mappers
{
    public class AutomapperProfile: Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Flight, FlightDTO>();
            CreateMap<Transport, TransportDTO>();
            CreateMap<VFlightTransport, VFlightTransportDTO>();
        }
    }
}
