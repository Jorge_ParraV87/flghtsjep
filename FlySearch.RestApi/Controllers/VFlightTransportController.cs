﻿using AutoMapper;
using FlySearch.Core.DTOs;
using FlySearch.Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlySearch.RestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VFlightTransportController : ControllerBase
    {
        private readonly IVflightTransportInterface _VflightInterface;
        private readonly IMapper _mapper;

        public VFlightTransportController(IVflightTransportInterface vflightTransportInterface, IMapper mapper)
        {
            _VflightInterface = vflightTransportInterface;
            _mapper = mapper;
        }

         [HttpGet]
        public async Task<List<VFlightTransportDTO>> GetFlight()
        {
            var result = await _VflightInterface.GetVFlights();

            var VflightDTO = _mapper.Map<List<VFlightTransportDTO>>(result);

            return VflightDTO;
        }

    }
}
