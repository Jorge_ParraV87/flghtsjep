﻿using FlySearch.Core.Entities;
using FlySearch.Core.Interfaces;
using FlySearch.Core.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace FlySearch.RestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlightController : ControllerBase 
    {
        private readonly IFlightInterface _flightInterface;
        private readonly IMapper _mapper;

        public FlightController(IFlightInterface flightInterface, IMapper mapper)
        {
            _flightInterface = flightInterface;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<List<FlightDTO>> GetFlight()
        {
            var result = await _flightInterface.GetFlights();

            var FlightDTO = _mapper.Map<List<FlightDTO>>(result);

            return FlightDTO;
        }

        [HttpGet("{id}")]
        public async Task<FlightDTO> GetFlightId(int id)
        {

                var result = await _flightInterface.GetFlightsId(id);

                var FlightIdResul = _mapper.Map<FlightDTO>(result);

                return FlightIdResul;            


        }
    }
}
