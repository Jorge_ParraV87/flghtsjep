﻿using System;
using System.Collections.Generic;

namespace FlySearch.Core.Entities
{
    public partial class Transport
    {
        public Transport()
        {
            Flight = new HashSet<Flight>();
        }

        public int IdTransport { get; set; }
        public string NameTransport { get; set; }
        public string FlightNumber { get; set; }

        public virtual ICollection<Flight> Flight { get; set; }
    }
}
