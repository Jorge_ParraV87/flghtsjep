﻿using System;
using System.Collections.Generic;

namespace FlySearch.Core.Entities
{
    public partial class Flight
    {
        public int IdFlight { get; set; }
        public string DepartureStation { get; set; }
        public string ArrivalStation { get; set; }
        public DateTime? DepartureDate { get; set; }
        public int? IdTransport { get; set; }
        public decimal? Price { get; set; }
        public string Currency { get; set; }

        public virtual Transport IdTransportNavigation { get; set; }
    }
}
