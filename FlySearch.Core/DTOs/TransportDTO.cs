﻿

namespace FlySearch.Core.DTOs
{
    public class TransportDTO
    {
        public int IdTransport { get; set; }
        public string NameTransport { get; set; }
        public string FlightNumber { get; set; }
    }
}
