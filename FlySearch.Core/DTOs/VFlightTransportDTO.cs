﻿using System;

namespace FlySearch.Core.DTOs
{
    public  class VFlightTransportDTO
    {

        public int IdFlight { get; set; }
        public string DepartureStation { get; set; }
        public string ArrivalStation { get; set; }
        public DateTime? DepartureDate { get; set; }
        public string DepDate { get; set; }
        public string DepHour { get; set; }
        public int? IdTransport { get; set; }
        public string NameTransport { get; set; }
        public string FlightNumber { get; set; }
        public decimal? Price { get; set; }
        public string Currency { get; set; }

    }
}
