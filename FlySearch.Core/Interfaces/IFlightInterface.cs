﻿using FlySearch.Core.DTOs;
using FlySearch.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FlySearch.Core.Interfaces
{
    public interface IFlightInterface
    {
        Task<List<FlightDTO>> GetFlights();

        Task<FlightDTO> GetFlightsId(int id);
    }
}
