﻿using FlySearch.Core.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlySearch.Core.Interfaces
{
    public interface IVflightTransportInterface
    {
        Task<List<VFlightTransportDTO>> GetVFlights();
    }
}
